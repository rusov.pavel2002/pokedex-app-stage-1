import { cards } from "./view/view"

const root = document.getElementById('root');
let isLoading: boolean = false

if (!root) {
    throw new Error('root not found');
}

const handleScroll = async () => {
    if (document.documentElement.scrollHeight < document.documentElement.scrollTop + window.innerHeight + 100 && !isLoading) {
        isLoading = true
        const newCards = await cards();
        root.appendChild(newCards);
        isLoading = false;
    }
};

document.addEventListener('scroll', handleScroll);

const render = async() => {
    const cardsElement = await cards();
    root.appendChild(cardsElement);
}

render()
