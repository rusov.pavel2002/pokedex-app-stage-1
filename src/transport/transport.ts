interface Pokemon {
    name: string;
    url: string;
}

interface PokemonApiResponse {
    count: number;
    next: string | null;
    previous: string | null;
    results: Pokemon[];
}

interface PokemonDetails {
    id: number;
    name: string;
    base_experience: number;
    height: number;
    is_default: boolean;
    order: number;
    weight: number;
    [key: string]: any;
}

interface PokemonFetchInterface {
    protocol: string,
    hostName: string,
    versionPath: string,
    route: string,
    offset?: number,
    limit?: number,
    timeout: number,
    cacheLimit: number

    changeRoute(route: string): void;
    getDataAboutPokemons(offset: number, limit: number): Promise<PokemonApiResponse> | never;
    getDetailsPokemon(pokemonName: string): Promise<PokemonDetails> | never;
}

export class PokemonFetch implements PokemonFetchInterface {
    private static instance: PokemonFetch;
    protocol: string = 'https';
    hostName: string = '://pokeapi.co';
    versionPath: string = '/api/v2/';
    route: string = 'pokemon/'
    offset: number = 0;
    limit: number = 100000;
    timeout: number = 10 * 1000; // 10 seconds
    cacheLimit: number = 1000000 * 1000; // 11 days
    private constructor() {}
  
    public static getInstance(): PokemonFetch {
        if (!PokemonFetch.instance) {
            PokemonFetch.instance = new PokemonFetch();
        }
        return PokemonFetch.instance;
    }

    public changeRoute(route: string): void {
        this.route = route;
    }

    public async getDataAboutPokemons(offset: number = 0, limit: number = 20) {
        const response = await fetch(`${this.protocol + this.hostName + this.versionPath + this.route}?offset=${offset}&limit=${limit}`);
        if (!response.ok) {
            throw new Error(`Error! status: ${response.status}`);
        }
        return await response.json()
    }

    public async getDetailsPokemon(pokemonName: string) {
        const response = await fetch(this.protocol + this.hostName + this.versionPath + this.route + pokemonName);
        if (!response.ok) {
            throw new Error(`Error! status: ${response.status}`);
        }
        return await response.json()
    }
}