import { PokemonService } from "@/service/service"
import { PokemonData } from "@/model/model";
import { paginationData } from "@/store/store";
import styles from "./view.scss";

// Add all functions which return html into this directory
const pokemonServiceInstance = PokemonService.getInstance();

export const cards = async () => {
    try {
        const cardsData: PokemonData[] = await pokemonServiceInstance.getDataFromApi();

        // создаю контейнер карт
        const cardsContainerHTML = document.createElement('div');
        cardsContainerHTML.classList.add(styles.cards__container);
    
        for (let index = paginationData.currentPokemons; index < paginationData.currentPokemons + 20; index++) {
            // создаю контейнер покемона
            const cardHTML = document.createElement('div');
            cardHTML.classList.add(styles.cards__item);

            // создаю аватарку покемона
            const cardImageHTML = document.createElement('img');
            cardImageHTML.classList.add(styles.card__image);
            cardImageHTML.src = cardsData[index].avatar;

            // создаю name покемона
            const cardNameHTML = document.createElement('h3');
            cardNameHTML.classList.add(styles.card__name);
            cardNameHTML.innerText = cardsData[index].name.toUpperCase();
    
            // создаю id покемона
            const cardIdHTML = document.createElement('h2');
            cardIdHTML.classList.add(styles.card__id);
            cardIdHTML.innerText = `Pokemon ID: ${cardsData[index].id.toString()}`;
    
            // создаю кнопку карточки
            const catchedBtnHTML = document.createElement('button');
            catchedBtnHTML.addEventListener('click', pokemonServiceInstance.clickedBtn);
            catchedBtnHTML.classList.add(styles.btn);
            catchedBtnHTML.setAttribute('data-id', cardsData[index].id.toString());
            catchedBtnHTML.innerText = 'Поймать!';
    
            // добавляю информацию о покемоне в карточку
            cardHTML.append(cardImageHTML, cardNameHTML, cardIdHTML, catchedBtnHTML);
    
            // добавляю карточку в контейнер ко всем карточкам
            cardsContainerHTML.append(cardHTML);
        }
        paginationData.currentPokemons += 20
        return cardsContainerHTML
    } catch (error) {
        console.log(error)
    }
}
